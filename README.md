vpa
=======

Chart source: https://github.com/FairwindsOps/charts/blob/master/stable/vpa 

# Introduction
To help customers define resource quota for K8S resources, the K8S Vertical Autoscaler (VPA) is installed.<br>
If a VPA CRD is installed in a namespace, the VPA operator populates it with statistics.<br>
VPA is run in recommendation-mode, but customers can decide to apply automatic resourcequota updates.<br>
The Goldilocks dashboard is not included, because it does not natively support multitenancy.<br>
See https://github.com/kubernetes/autoscaler/tree/master/vertical-pod-autoscaler for details on using VPA, with examples.
